package sample;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("sql_task");
        primaryStage.setScene(new Scene(root, 542, 574));
        primaryStage.show();


//
//        FXMLLoader loader = new FXMLLoader(getClass().getResource("sample.fxml"));
//
//        Controller controller = (Controller)loader.getController();
//
//        System.out.println(controller.searchBy);
    }


    public static void main(String[] args) {
        launch(args);
    }
}
